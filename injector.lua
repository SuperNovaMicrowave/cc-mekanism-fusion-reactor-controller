local device_name = "injector_controller"

local d_modemSide = "back"
local d_tankFlowSide = "left"
local d_hohlraumInsertionSide = "right"
local d_hohlraumRefillingSide = "top"

local slave_networking = require("networking.slave")
local common_networking = require("networking.common")

-- if no settings available load defaults
local settings_path = "/injector.settings"
if settings.load(settings_path) ~= true then
    settings.set("modemSide", d_modemSide)
    settings.set("tankFlowSide", d_tankFlowSide)
    settings.set("hohlraumInsertionSide", d_hohlraumInsertionSide)
    settings.set("hohlraumRefillingSide", d_hohlraumRefillingSide)
    
    settings.save(settings_path)
end

local protocols = common_networking.getProtocols()

local controller_networking_id = slave_networking.init(protocols.sync, settings.get("modemSide", d_modemSide), device_name)

local actions = {
    ["pushHohlraum"] = function() 
        redstone.setOutput(settings.get("hohlraumRefillingSide", d_hohlraumRefillingSide), true) -- disable export bus
        sleep(0.2)
        redstone.setOutput(settings.get("hohlraumInsertionSide", d_hohlraumInsertionSide), true)
        sleep(0.2)
        redstone.setOutput(settings.get("hohlraumInsertionSide", d_hohlraumInsertionSide), false)
        sleep(0.1)
        redstone.setOutput(settings.get("hohlraumRefillingSide", d_hohlraumRefillingSide), false) -- re-enable export bus
    end,
    ["enableFuelFlow"] = function() 
        redstone.setOutput(settings.get("tankFlowSide", d_tankFlowSide), true)
    end,
    ["disableFuelFlow"] = function() 
        redstone.setOutput(settings.get("tankFlowSide", d_tankFlowSide), false)
    end,
}

while true do
    local senderId, message, protocol = rednet.receive(protocols.injector)
    actions[message]()
end