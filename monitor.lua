local device_name = "monitor_controller"

local d_modemSide = "bottom"
local d_monitorSide = "top"

local slave_networking = require("networking.slave")
local common_networking = require("networking.common")

-- if no settings available load defaults
local settings_path = "/monitor.settings"
if settings.load(settings_path) ~= true then
    settings.set("modemSide", d_modemSide)
    settings.set("monitorSide", d_monitorSide)
    
    settings.save(settings_path)
end

local protocols = common_networking.getProtocols()

local controller_networking_id = slave_networking.init(protocols.sync, settings.get("modemSide", d_modemSide), device_name)

