local function connectInjector(injector_protocol, injector_controller_id)
    local injector = {}
    
    injector["pushHohlraum"] = function()
        rednet.send(injector_controller_id, "pushHohlraum", injector_protocol)
    end
    
    injector["enableFuelFlow"] = function(enable)
        if enable then
            rednet.send(injector_controller_id, "enableFuelFlow", injector_protocol)
        else
            rednet.send(injector_controller_id, "disableFuelFlow", injector_protocol)
        end
    end
    
    return injector
end

return {connectInjector = connectInjector}