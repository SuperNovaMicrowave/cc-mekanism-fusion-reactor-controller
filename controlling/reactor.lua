local function connectReactor()
    local reactor = {}
    
    peripheral.find("Reactor Logic Adapter", function(n, device)
        reactor["getWater"] = device.getWater
        reactor["getSteam"] = device.getSteam
        
        reactor["hasFuel"] = device.hasFuel
        reactor["getFuel"] = device.getFuel
        reactor["getDeuterium"] = device.getDeuterium
        reactor["getTritium"] = device.getTritium
        
        reactor["getMaxEnergy"] = device.getMaxEnergy
        reactor["getEnergy"] = device.getEnergy
        reactor["getProducing"] = device.getProducing
        
        reactor["getMaxCaseHeat"] = device.getMaxCaseHeat
        reactor["getCaseHeat"] = device.getCaseHeat
        
        reactor["getPlasmaHeat"] = device.getPlasmaHeat
        reactor["getMaxPlasmaHeat"] = device.getMaxPlasmaHeat
        
        reactor["getInjectionRate"] = device.getInjectionRate
        reactor["setInjectionRate"] = device.setInjectionRate
        
        reactor["canIgnite"] = device.canIgnite
        reactor["isIgnited"] = device.isIgnited
        reactor["getIgnitionTemp"] = device.getIgnitionTemp
    end)
    
    return reactor
end

return {connectReactor = connectReactor}