local function connectLaser(laser_protocol, trigger_controller_id)
    local laser = {}
    
    peripheral.find("Laser Amplifier", function(n, device)
        laser["getEnergy"] = device.getEnergy
        laser["getMaxEnergy"] = device.getMaxEnergy
    end)
    
    laser["fire"] = function()
        rednet.send(trigger_controller_id, "fire", laser_protocol)
    end
    
    laser["setCharging"] = function(enable)
        if enable then
            rednet.send(trigger_controller_id, "enableCharging", laser_protocol)
        else
            rednet.send(trigger_controller_id, "disableCharging", laser_protocol)
        end
    end
    
    return laser
end

return {connectLaser = connectLaser}