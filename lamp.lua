local device_name = "lamp_controller"

local d_modemSide = "top"
local d_lampSide = "bottom"

local slave_networking = require("networking.slave")
local common_networking = require("networking.common")

-- if no settings available load defaults
local settings_path = "/lamp.settings"
if settings.load(settings_path) ~= true then
    settings.set("modemSide", d_modemSide)
    settings.set("lampSide", d_lampSide)
    
    settings.save(settings_path)
end

local protocols = common_networking.getProtocols()

local controller_networking_id = slave_networking.init(protocols.sync, settings.get("modemSide", d_modemSide), device_name)

while true do
	local senderId, message, protocol = rednet.receive(protocols.lamp)
	local count, tOn, tOff = message[1], message[2], message[3]
	for i = 1, tonumber(count) do
		redstone.setOutput(settings.get("lampSide", d_lampSide), true)
		sleep(tOn)
		redstone.setOutput(settings.get("lampSide", d_lampSide), false)
		sleep(tOff)
	end
end