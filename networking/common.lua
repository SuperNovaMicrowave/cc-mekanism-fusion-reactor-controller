local function getProtocols()
    return {
        sync = "NetworkSync",
        status = "StatusBroadcast",
        settings = "SettingsUpdate",
        laser = "LaserControl",
        injector = "InjectionControl",
        lamp = "HazardLamp"
    }
end

return {getProtocols = getProtocols}