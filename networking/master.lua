local function init(syncProtocol, modemSide)
    rednet.open(modemSide)
    
    slaves = {}
    
    peripheral.find("computer", function(n, slave)
        if slave.isOn() then
            slave.shutdown()
            sleep(2)
        end
        slave.turnOn()
        sleep(2)
        
        local slave_id = slave.getID()
        
        rednet.send(slave_id, "identify", syncProtocol)
        local return_id, slave_name, return_protocol = rednet.receive(syncProtocol)
        
        slaves[slave_name] = slave_id
    end)
    
    print("this computer's id:", os.getComputerID())
    table.foreach(slaves, function(key, value)
        print("found slave", key, "at", value)
    end)
    
    return slaves.injector_controller, slaves.lamp_controller, slaves.monitor_controller, slaves.trigger_controller
end

return {init = init}