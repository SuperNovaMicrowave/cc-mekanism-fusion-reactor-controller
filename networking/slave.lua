local function init(syncProtocol, modemSide, deviceName)
    rednet.open(modemSide)
    
    local master_id, message, returnProtocol = rednet.receive(syncProtocol)
    sleep(3)
    if message == "identify" then
        rednet.send(master_id, deviceName, returnProtocol)
    end
    
    print("this computer's id:", os.getComputerID())
    print("found master at", master_id)
    
    return master_id
end

return {init = init}