local device_name = "trigger_controller"

local d_modemSide = "bottom"
local d_laserAmplifierSide = "left"
local d_laserEnergyBufferSide = "right"

local slave_networking = require("networking.slave")
local common_networking = require("networking.common")

-- if no settings available load defaults
local settings_path = "/trigger.settings"
if settings.load(settings_path) ~= true then
    settings.set("modemSide", d_modemSide)
    settings.set("laserAmplifierSide", d_laserAmplifierSide)
    settings.set("laserEnergyBufferSide", d_laserEnergyBufferSide)
    
    settings.save(settings_path)
end

local protocols = common_networking.getProtocols()

local controller_networking_id = slave_networking.init(protocols.sync, settings.get("modemSide", d_modemSide), device_name)

local actions = {
    ["fire"] = function() 
        redstone.setOutput(settings.get("laserAmplifierSide", d_laserAmplifierSide), true)
        sleep(0.1)
        redstone.setOutput(settings.get("laserAmplifierSide", d_laserAmplifierSide), false)
    end,
    ["enableCharging"] = function() 
        redstone.setOutput(settings.get("laserEnergyBufferSide", d_laserEnergyBufferSide), true)
    end,
    ["disableCharging"] = function() 
        redstone.setOutput(settings.get("laserEnergyBufferSide", d_laserEnergyBufferSide), false)
    end,
}

while true do
    local senderId, message, protocol = rednet.receive(protocols.laser)
    actions[message]()
end