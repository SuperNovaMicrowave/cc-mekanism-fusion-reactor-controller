local d_modemSide = "bottom"
local d_controlMonitorSide = "right"

local master_networking = require("networking.master")
local common_networking = require("networking.common")

local reactor_controlling = require("controlling.reactor")
local laser_controlling = require("controlling.laser")

local fuel_controlling = require("controlling.fuel")

-- if no settings available load defaults
local settings_path = "/controller.settings"
if settings.load(settings_path) ~= true then
    settings.set("modemSide", d_modemSide)
    settings.set("controlMonitorSide", d_controlMonitorSide)
    
    settings.save(settings_path)
end

local protocols = common_networking.getProtocols()

local injector_controller_networking_id, lamp_controller_networking_id, monitor_controller_networking_id, trigger_controller_networking_id = master_networking.init(protocols.sync, settings.get("modemSide", d_modemSide))

local reactor = reactor_controlling.connectReactor()
local laser = laser_controlling.connectLaser(protocols.laser, trigger_controller_networking_id)

local injector = fuel_controlling.connectInjector(protocols.injetor, injector_controller_networking_id)

local controlMonitor = peripheral.wrap(settings.get("controlMonitorSide", d_controlMonitorSide))

local statusMonitor = {}
peripheral.find("monitor", function(n, device) 
    statusMonitor = device
end)

local status = {
    ["reactor"] = {
        ["water"] = 0,
        ["steam"] = 0,
        ["fuel"] = 0,
        ["deuterium"] = 0,
        ["tritium"] = 0,
        ["energy"] = {
            ["stored"] = 0,
            ["producing"] = 0,
            ["max"] = 0,
        },
        ["caseHeat"] = {
            ["max"] = 0,
            ["current"] = 0,
        },
        ["plasmaHeat"] = {
            ["max"] = 0,
            ["current"] = 0,
        },
        ["injectionRate"] = 0,
        ["ready"] = false,
        ["running"] = false,
        ["shouldBeRunning"] = reactor.isIgnited(),
    },
    ["laser"] = {
        ["energy"] = {
            ["stored"] = 0,
            ["max"] = 0,
        },
    }
}

local pollValues = function()
    status.reactor.water = reactor.getWater()
    status.reactor.steam = reactor.getSteam()
    status.reactor.fuel = reactor.getFuel()
    status.reactor.deuterium = reactor.getDeuterium()
    status.reactor.tritium = reactor.getTritium()
    status.reactor.energy.stored = reactor.getEnergy()
    status.reactor.energy.producing = reactor.getProducing()
    status.reactor.energy.max = reactor.getMaxEnergy()
    status.reactor.caseHeat.max = reactor.getMaxCaseHeat()
    status.reactor.caseHeat.current = reactor.getCaseHeat()
    status.reactor.plasmaHeat.max = reactor.getMaxPlasmaHeat()
    status.reactor.plasmaHeat.current = reactor.getPlasmaHeat()
    status.reactor.injectionRate = reactor.getInjectionRate()
    status.reactor.ready = reactor.canIgnite()
    status.reactor.running = reactor.isIgnited()
    
    status.laser.energy.stored = laser.getEnergy()
    status.laser.energy.max = laser.getMaxEnergy()
end

local mainLoop = function()
    if status.reactor.running then
        if status.reactor.shouldBeRunning then -- normal opteration
            
            
        else -- initiate shutdown
            
            
        end
    else
        if status.reactor.shouldBeRunning then -- initiate startup
            
            
        else -- off
            
            
        end
    end
end

local statusUpdate = function()
    rednet.broadcast(status, protocols.status)
end

local threads = {
    ["tick"] = function()
        while true do
            pollValues()
            mainLoop()
            statusUpdate()
        end
    end,
    ["control_screen"] = function()
        -- setup screen buttons
        controlMonitor.clear()
        controlMonitor.setCursorPos(2, 2)
        controlMonitor.setBackgroundColor(colors.green)
        controlMonitor.write("  |  ")
        controlMonitor.setCursorPos(2, 4)
        controlMonitor.setBackgroundColor(colors.red)
        controlMonitor.write("  O  ")
        
        -- check for presses
        while true do
            local touch_event, event_side, xPos, yPos = os.pullEvent("monitor_touch")
            if xPos <= 6 and xPos >= 2 then
                if yPos == 2 then -- on button pressed
                    status.reactor.shouldBeRunning = true
                elseif yPos == 4 then -- off button pressed
                    status.reactor.shouldBeRunning = false
                end
            end
        end
    end,
    ["settings_receiver"] = function()
        while true do
            local senderId, message, protocol = rednet.receive(protocols.settings)
        end
    end,
}

parallel.waitForAny(threads.tick, threads.control_screen, threads.settings_receiver)