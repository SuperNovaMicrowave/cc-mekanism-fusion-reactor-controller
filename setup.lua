﻿-- pastebin: 35MtLyvs

local baseUrl = "https://gitlab.com/SuperNovaMicrowave/cc-mekanism-fusion-reactor-controller/-/raw/master/"

local network_library_files = {
    {baseUrl .. "networking/common.lua", "/networking/common.lua"},
    {baseUrl .. "networking/master.lua", "/networking/master.lua"},
    {baseUrl .. "networking/slave.lua", "/networking/slave.lua"}
}

local controlling_library_files = {
    {baseUrl .. "controlling/fuel.lua", "/controlling/fuel.lua"},
    {baseUrl .. "controlling/laser.lua", "/controlling/laser.lua"},
    {baseUrl .. "controlling/reactor.lua", "/controlling/reactor.lua"}
}

local startup_files = {
    ["controller"] = baseUrl .. "controller.lua",
    ["injector"] = baseUrl .. "injector.lua",
    ["lamp"] = baseUrl .. "lamp.lua",
    ["monitor"] = baseUrl .. "monitor.lua",
    ["trigger"] = baseUrl .. "trigger.lua",
}

local download = function(url, file)
  local pulledFile = http.get(url).readAll()
  file = fs.open(file, "w")
  file.write(pulledFile)
  file.close()
end

local purge_prev_install = function()
    fs.delete("/startup.lua")
    fs.delete("/networking")
    fs.delete("/controlling")
end

local common_setup_routine = function()
    table.foreach(network_library_files, function(n, listEntry)
        download(listEntry[1], listEntry[2])
    end)
end

local device_specific_setup_routines = {
    [1] = function()
        table.foreach(controlling_library_files, function(n, listEntry)
            download(listEntry[1], listEntry[2])
        end)
        download(startup_files.controller, "/startup.lua")
    end,
    [2] = function()
        download(startup_files.injector, "/startup.lua")
    end,
    [3] = function()
        download(startup_files.lamp, "/startup.lua")
    end,
    [4] = function()
        download(startup_files.monitor, "/startup.lua")
    end,
    [5] = function()
        download(startup_files.trigger, "/startup.lua")
    end,
}

print("Select device:")
print("1: reactor controller")
print("2: injector controller")
print("3: lamp controller")
print("4: monitor controller")
print("5: laser controller")

local deviceSelection = tonumber(read())

purge_prev_install()
common_setup_routine()
device_specific_setup_routines[deviceSelection]()